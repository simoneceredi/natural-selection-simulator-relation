\babel@toc {italian}{}
\babel@toc {italian}{}
\contentsline {chapter}{\numberline {1}Analisi}{2}{chapter.1}
\contentsline {section}{\numberline {1.1}Requisiti}{2}{section.1.1}
\contentsline {section}{\numberline {1.2}Analisi e modello del dominio}{3}{section.1.2}
\contentsline {chapter}{\numberline {2}Design}{5}{chapter.2}
\contentsline {section}{\numberline {2.1}Architettura}{5}{section.2.1}
\contentsline {section}{\numberline {2.2}Design dettagliato}{6}{section.2.2}
\contentsline {chapter}{\numberline {3}Sviluppo}{24}{chapter.3}
\contentsline {section}{\numberline {3.1}Testing automatizzato}{24}{section.3.1}
\contentsline {section}{\numberline {3.2}Metodologia di lavoro}{25}{section.3.2}
\contentsline {section}{\numberline {3.3}Note di sviluppo}{26}{section.3.3}
\contentsline {chapter}{\numberline {4}Commenti finali}{29}{chapter.4}
\contentsline {section}{\numberline {4.1}Autovalutazione e lavori futuri}{29}{section.4.1}
\contentsline {section}{\numberline {4.2}Difficolt\IeC {\`a} incontrate e commenti per i docenti}{31}{section.4.2}
\contentsline {chapter}{\numberline {A}Guida utente}{33}{appendix.Alph1}
