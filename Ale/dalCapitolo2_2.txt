-1 ANALISI

1.1 Requisiti
Il gruppo si pone come obiettivo quello di realizzare un simulatore che visualizzi l'evoluzione di una popolazione di individui nel tempo a seguito di adattamenti generazionali che possono colpire ogni individuo nel momento della nascita in maniera casuale.
Il progetto si ritiene utile per visualizzare come una data popolazione, che possiede determinate caratteristiche e proprietà iniziali si evolva nel tempo per meglio adattarsi all'ambiente in cui vive. 

Requisiti funzionali:
- Durante il giorno gli individui dovranno muoversi alla ricerca di cibo il quale si troverà disposto in posizioni casuali all' interno dell'ambiente. Essi potranno muoversi e perderanno energia in base alle loro caratteristiche.
- Cibandosi essi acquistano una quantità di energia pari al valore energetico offerto dal cibo.
- Durante la notte le entità possono procreare. Ogni entità figlia avrà le stesse caratteristiche del padre più una eventuale mutazione.
- La simulazione procede fino a quando tutti gli organismi si estinguono oppure infinitamente se le condizioni della popolazione e dell'ambiente lo permettono.
- Analisi quantitativa dell'evoluzione degli organismi in vita tramite grafici in real-time.
Requisiti non funzionali:
- Progettare il simulatore in modo tale da renderlo facilmente espandibile con altre mutazioni o con altri elementi dell'ambiente.
- L'applicativo dovrà essere efficiente nell'uso delle risorse.

1.2 Analisi e modello del dominio

La simulazione ha luogo in un Environment il quale possiede definite caratteristiche quali la temperatura e la dimensione. Esso viene popolato da Organismi e Cibo. Gli organismi sono contraddistinti da Trait, caratteri specifici:
- Dimensione: consente di stabilire quanto cibo un organismo può mangiare. Un organismo di dimensione maggiore sarà in grado di mangiare di più però consumerà più energie nel movimento. La dimensione influisce anche sulla resistenza alla temperatura, un'individuo più grande sarà in grado di resistere meglio al freddo.
- Velocità: maggiore velocita aumenta il consumo di energie dell'organismo nel movimento.
- Numero di figli generabili
- Food Radar: capacità di individuare e mangiare cibo in un'area attorno a sè. Maggiore questa sensibilità più energie consumerà l'organismo.
Durante la procreazione i figli possono incorrere in mutazioni dei loro caratteri; questo porta all'evoluzione della specie. Solamente gli organismi con caratteristiche affini all'ambiente in cui si trovano a vivere potranno sopravvivere. Disseminati nell'environment gli organismi possono trovare del cibo il qule possiede una ben definita quantità di energia.
Gli elementi costitutivi il problema sono sintetizzati in *FIGURA*.
Il requisito non funzionale riguardante efficienza nell'uso delle risorse non potrà essere svolto all'interno del monte ore previsto in quanto prevede un analisi dettagliata delle performance dell'applicativo: tale feature sarà oggetto di futuri lavori.

2 DESIGN

2.1 Architettura
Per la realizzazione di Natural Selection Simulator abbiamo scelto di utilizzare il pattern
architetturale Model-View-Controller. In questo modo è stato possibile isolare la logica funzionale che riguarda le tre componenti, le quali non interferiscono tra loro. Il progetto risulta essere così meglio organizzato e la modifica di una delle componenti non comporta
cambiamenti nelle restanti.
Nella nostra modellazione del pattern MVC il controller comanda le modifiche al modello e cambia di conseguenza la view. In particolare il controller gestisce il loop di simulazione aggiornando model (modello object-orented del dominio applicativo) e notificando alla view i cambiamenti da effettuare periodicamente.
Il controller gestendo il coordinamento tra model e view si interpone tra questi, in questo modo le tre componenti rimangano estremamente separate, permettendo una sostituzione di view o model senza impattare il funzionamento del sistema.

2.2 Design dettagliato

Il mio compito nel progetto è stato quello di modellare le entità (organismi e cibo) che popolano l'ambiente e in seguito gestire le azioni che questi svolgeranno durante il loro ciclo vitale.

Le entità che popolano l'ambiente: organismi e cibo, presentano alcuni tratti caratteristici comuni tra loro; uno di questi è il fatto che entrambi possiedono un livello di energia. Per ottemperare a questo fatto ho deciso di creare un'entità padre, comune ad entrambi che raggruppasse quelle che sono le loro caratteristiche comuni.
Ciò è stato fatto con un'interfaccia Entity, la quale assieme alla relativa classe astratta AbstractEntity rappresenta l'ossatura centrale delle entità in campo.
Organismi e Cibo perciò non sono altro che estensioni della classe AbstractEntity raffinate con i relativi metodi propri che le caratterizzano. *FIGURA*
Per istanziare tali elementi ho ritenuto utile fornire due Builder: uno per il cibo (FoodBuilder) e uno per gli organismi (OrganismBuilder). Ritengo tale scelta opportuna in quanto: nel caso del cibo, in scenari futuri potrebbero voler essere inserite varie tipologie di cibo, le quali differiscono per esempio per quantità di energia conferita.
Per quanto riguarda il Builder degli organismi le considerazioni che mi hanno portato alla scelta di tale pattern costruttivo sono state dettate dal fatto che un organismo potrebbe teoricamente possedere un numero indefinito di caratteristiche, soprattutto tenendo presente come in futuro possano voler essere aggiunti alcuni caratteri piuttosto che altri. Grazie a questa scelta in futuro potranno essere istanziati anche diverse "specie" di organismi i quali possiedono caratteristiche diverse a seconda della specie di cui fanno parte.
Descriverò solamente il Builder relativo all'organismo in quanto lo ritengo di maggior rilievo, per il cibo ho agito analogamente.
Con riferimento alla *FIGURA*: il Builder in se è l'interfaccia OrganismBuilder che definisce i metodi per settare le varie parti che compongono l'oggetto che deve essere costruito.
Ad implementare tale interfaccia e quindi a costruire effettivamente gli oggetti è stata creata la classe OrganismBuilderImpl. L'energia viene presa in ingresso dal costruttore in quanto elemento fondamentale per la costruzione di un organismo.

L'idea dietro alla simulazione è che vi sia un ciclo giornaliero. Un'alternanza tra giorno e notte definisce infatti quali azioni devono essere compiute dagli individui nei vari momenti della giornata.
L'ambiente (gestore dell'alternanza temporale) notifica di volta in volta ai singoli organismi quale ciclo di azioni intraprendere a seconda del momento della giornata in cui si trovano.
Sono state attualmente implementate due cicli di azioni, il ciclo delle azioni del giorno e quelle svolte durante la notte. Le azioni del giorno permettono ad un organismo di muoversi e di mangiare il cibo che trova nell'ambiente, mentre durante la notte potrà generare entità figlie.
Dal momento in cui i periodi si differenziano soltanto in base a quali compiti vengono portati a termine in una situazione piuttosto che nell'altra, ho agito in modo da lasciare piena liberà alle sottoclassi di implementare il comportamento che differisce e massimizzare il riuso.  
Per fare ciò ho utilizzato il pattern Command.
Con riferimento alla *FIGURA*: l'interfaccia Actions svolge il compito di command, dichiarando l'operazione execute() da svolgere, mentre DayAction e NightAction rappresentano gli effettivi concreteCommand i quali implementano perform() invocando le specifiche operazioni da svolgere nelle differenti situazioni.

Per la gestione dei compiti assegnati ad ogni organismo si utilizza il pattern Strategy.
Come da *FIGURA*: le implementazioni dei singoli compiti possono essere così facilmente modificate, e la modifica impatta direttamente sul comportamento di ogni organismo.


3 SVILUPPO
3.1 Testing automatizzato
Organism: viene testata la creazione di un organismo tramite apposito Builder, sottoposti a test anche casi fallaci che determinano eccezioni. Verifica dei traits inseriti e della quantità di energia.
Food: testata la creazione di un food tramite apposito Builder e verificato lo stato di energia.
Action: vengono testate alcune casistiche delicate che possono risultare indice di errore durante il funzionamento dell'applicazione e passare inosservate; soprattutto nei casi in cui vi è una maggiorazione di organismi in vita.

3.2 Metodologia di lavoro
A seguito della fase di sviluppo, possiamo asserire che la ripartizione dei compiti da noi concordata inizialmente e confermata nelle fasi precedenti è stata rispettata, risultando equa e piuttosto bilanciata. Non sono state necessarie sostanziali modifiche e le dipendenze tra le nostre parti sono limitate. 
Ciò è dovuto al fatto che parte di analisi architetturale è stata svolta assieme in un momento antecedente allo sviluppo, anche utilizzando strumenti di connessione virtuale quali "Skype" e "lucidchart.com", in tal modo abbiamo definito quali interfacce usare e solamente in un secondo momento ci siamo occupati in maniera indipendente di sviluppare le differenti parti del sistema.

Di seguito elenchiamo le funzionalità implementate dai singoli elementi del team:
- Conti Alessio: gestione delle entità della simulazione (organismi e cibo),  gestione delle azioni svolte dagli organismi durante le varie fasi della giornate, gestione dei movimenti.
........
Le parti comuni tra i componenti del team:
- Aggiornamento della simulazione Ceredi Simone, Conti Alessio per quanto riguarda l'aggiornamento di ambiente, organismo e cibo, Giulianelli Andrea per la parte che concerne l'aggiornamento in real time dei grafici.
Le classi interessate sono SimulationController la quale gestisce la scena della simulazione e SimulationViewLogics corredata da relativa implementazione per gestire il render della canvas.

Nella fase di sviluppo abbiamo utilizzato il DVCS Git.
Abbiamo creato una repository su BitBucket e ciascun membro del team ha effettuato la clone.
Sul branch develop è stata sempre mantenuta una versione aggiornata del codice che fosse priva di errori di compilazione. Ogni componente ha lavorato autonomamente sul proprio repository in locale, condividendo su develop tramite push al termine di ogni task a meno che non ci fossero errori di compilazione.
Sul branch master è stata aggiunta solo la versione funzionante dell'applicazione.

3.3 Note sullo sviluppo
Conti Alessio
- Stream: uso degli stream per la gestione di collezioni.
- Lambda expression: ho specificato alcune lamda expressions con l'obiettivo di avere codice più compatto e leggibile, spesso in concomitanza di stream; in particolare nelle classi relative compiti assegnati ad ogni organismo.
- Optional: in ogni caso in cui una classe poteva avere delle informazioni assenti è stato scelto di usare Optional.
- Libreria Apache Commons: per non ricreare classi già implementate, limitatamente all'utilizzo di Pair.
- JavaFx: per gestire la parte relativa alla GUI del progetto.
Per quanto riguarda pattern progettuali ho consultato il materiale didattico delle lezioni in aula, approfondendo il tutto con il libro "Design Patterns: Elements of Reusable Object-Oriented-Software" - Gamma, Helm, Johnson, Vlissides. Grazie a tale testo sono entrato a conoscenza del pattern Command, utilizzato per gestire le azioni che devono essere svolte durante i vari periodi della giornata dagli organismi.


4 COMMENTI FINALI

4.1 Autovalutazione e lavori futuri
Conti Alessio
Sono personalmente soddisfatto del mio lavoro svolto all'interno del progetto. Fin dall'inizio è stato un progetto ambizioso e complesso ma proprio per questo interessante e stimolante.
Essendo la prima volta che affrontavo un progetto di tale portata e la prima volta ad affrontarlo in gruppo non nascondo il fatto che in un primo momento le incertezze sono state svariate e di differente natura. Tuttavia lavorare con un metodo ferreo e progettare l'ossatura del progetto prematuramente in maniera dettagliata per quanto concerne le parti che avrebbero dovuto in futuro collegarsi tra loro ha giocato un ruolo decisivo nel riuscito sviluppo del progetto senza causare troppi rallentamenti nel momento di scrittura del codice; confermando quanto detto dai professori a lezione: una buona progettazione alla base è la chiave per un riuscito progetto. 
Sono soddisfatto della mia parte del progetto. Ho cercato sempre di tenere alta la riusabilità ed estendibilità del codice anche grazie a diversi pattern sia costruttivi che di design. Sono consapevole del fatto che alcune implementazioni possano non essere le migliori, ma ritengo di aver messo in pratica tutte le conoscenze che al momento posseggo e di aver fatto del mio meglio in ogni sezione di cui mi sono occupato.
Ritengo di aver raggiunto un buon punto di estendibilità del codice, a riprova di ciò posso affermare che è risultato molto semplice e poco dispendioso implementare alcune funzionalità aggiuntive che non erano previste tra quelle ritenute obbligatorie. Per esempio aggiungendo il trait "FoodRadar", mi è bastato modificare alcune parti relative alla logica del movimento per giungere al risultato desiderato senza compromettere componenti terzi.
Considero tale progetto una solida base di partenza per poterci lavorare in futuro. Le possibilità di arricchirlo sono svariate: dall'aggiunta di caratteristiche proprie degli Organismi a caratteristiche riguardanti l'ambiente in cui vivono.
Può essere interessante modellare il concetto di specie di individuo, ognuna con caratteristiche proprie, e farle convivere nello stesso ambiente così da studiare e mettere in campo una vera e propria "lotta alla sopravvivenza". Allo stesso modo mettendo a disposizione varie tipologie di cibi si potrebbe modellare una sorta di AI propria degli organismi che gli permetta di scegliere il cibo "migliore" da ingerire in base alle caratteristiche e alle necessità della propria specie di appartenenza.
Inoltre sarebbe interessante estendere l'applicazione con opzioni aggiuntive e maggiormente dettagliate per lo studio dell'evoluzione degli organismi, in modo da poter interagire e meglio esaminare l'andamento di essa.

4.2 Difficoltà incontrate e commenti per i docenti

GUIDA UTENTE
Avviando la simulazione si incorre nella schermata in *IMMAGINE*:
E' possibile scegliere alcuni valori iniziali dal menù (1)
- Food Quantity: quantità di cibo disponibile nell'ambiente
- Food Variation: variazione di cibo giornaliera
- Initial Quantity: quantità iniziale di individui
- Temperatura: temperatura dell'ambiente
- Dimension: dimensione iniziale degli individui
- Speed: velocità inziale degli individui
Nella parte inferiore destra della schermata, (2) l'utente trova tre pulsanti.
- Help: descrive le informazioni riguardanti gli organismi durante la simulazione
- Restore default: per reimpostare i valori iniziali
- Start: inizia la simulazione
Alla pressione del tasto start la schermata visualizzata sarà similare all'*IMMAGINE*
Nella parte centrale verranno visualizzati in verde le unità di cibo, mentre in varie gradazioni di rosso gli organismi.
L'individuo a seconda dei valori assunti dalle varie caratteristiche cambia la sua rappresentazione:
Il raggio dell'individuo è direttamente proporzionale alla sua dimensione.
Velocita (Rosso), Food Radar (Marrone) e Children Quantity (Viola) influiscono sul colore, in particolare: più il valore della caratteristica è basso e più incideranno con una tonalità chiara del colore associato, contrariamente più il valore aumenta e più la tonalità diventa scura.
I tasti nella parte inferiore (1,2,3) permettono di gestire la simulazione.
(1) Permette di mettere in pausa e far ripartire la simulazione.
(2) Permette di tornare al menù iniziale *IMMAGINE SOPRAnale*.
(3) Consente di aprire la schermata delle impostazioni *IMMAGINE SOTTO*.
Nella parte destra (4) vengono visualizzati i grafici sull'andamento medio dei caratteri dell'intera popolazione.
Infine nella parte superiore (5) vengono visualizzate alcune informazioni riguardanti sulla popolazione in vita.
La schermata delle impostazioni *IMMAGINE* permette di settare la velocità di aggiornamento della simulazione. (1) 